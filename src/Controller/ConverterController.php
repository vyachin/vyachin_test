<?php

namespace App\Controller;

use App\Entity\Converter;
use App\Form\ConverterType;
use App\Service\CurrencyCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConverterController extends AbstractController
{
    #[Route('/converter', name: 'app_converter')]
    public function converter(Request $request, CurrencyCalculator $currencyCalculator): Response
    {
        $converter = new Converter ($currencyCalculator);
        $form = $this->createForm(ConverterType::class, $converter);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $result = $converter->calculateResult();
        } else {
            $result = null;
        }

        return $this->render('converter/index.html.twig', [
            'rate_form' => $form,
            'result' => $result,
        ]);
    }
}
