<?php

namespace App\Dto;

class RateDto
{
    public function __construct(
        public readonly string $src,
        public readonly string $dst,
        public readonly float $rate,
    ) {
    }

    public function __toString(): string
    {
        return "{$this->src} {$this->dst} {$this->rate}";
    }
}
