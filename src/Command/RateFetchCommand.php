<?php

namespace App\Command;

use App\Entity\Rate;
use App\Repository\RateRepository;
use App\Service\AdapterManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:rate-fetch',
    description: 'Fetch currency rates',
)]
class RateFetchCommand extends Command
{
    public function __construct(
        private readonly AdapterManager $adapterManager,
        private readonly RateRepository $rateRepository,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        foreach ($this->adapterManager->fetchRates() as $rateDto) {
            echo $rateDto, PHP_EOL;
            $rate = $this->rateRepository->findOneBy(['src' => $rateDto->src, 'dst' => $rateDto->dst]);
            if (!$rate) {
                $rate = new Rate();
                $rate->setSrc($rateDto->src)->setDst($rateDto->dst);
            }
            $rate->setRate($rateDto->rate);
            $this->rateRepository->save($rate, true);
        }

        $io->success('Complete!');

        return Command::SUCCESS;
    }
}
