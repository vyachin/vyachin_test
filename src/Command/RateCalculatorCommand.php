<?php

namespace App\Command;

use App\Service\CurrencyCalculator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:rate-calculator',
    description: 'Currency calculator',
)]
class RateCalculatorCommand extends Command
{
    public function __construct(private readonly CurrencyCalculator $currencyCalculator)
    {
        parent::__construct(null);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('src', InputArgument::REQUIRED, 'Source currency')
            ->addArgument('dst', InputArgument::REQUIRED, 'Destination currency')
            ->addArgument('sum', InputArgument::REQUIRED, 'Sum');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $src = $input->getArgument('src');
        $dst = $input->getArgument('dst');
        $sum = $input->getArgument('sum');

        $result = $this->currencyCalculator->calculate($src, $dst, $sum);

        $io->success("From: $src To: $dst Sum: $sum Result: $result");

        return Command::SUCCESS;
    }
}
