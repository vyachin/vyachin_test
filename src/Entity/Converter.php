<?php

namespace App\Entity;

use App\Service\CurrencyCalculator;

class Converter
{
    private ?string $src;
    private ?string $sum;
    private ?string $dst;
    private ?string $submit;

    public function __construct(
        private readonly CurrencyCalculator $currencyCalculator,
    ) {
    }

    public function getSrc(): ?string
    {
        return $this->src;
    }

    public function setSrc(string $src): self
    {
        $this->src = $src;

        return $this;
    }

    public function getDst(): ?string
    {
        return $this->dst;
    }

    public function setDst(string $dst): self
    {
        $this->dst = $dst;

        return $this;
    }

    public function getSum(): ?string
    {
        return $this->sum;
    }

    public function setSum(string $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getSubmit(): ?string
    {
        return $this->submit;
    }

    public function setSubmit(string $submit): self
    {
        $this->submit = $submit;

        return $this;
    }

    public function calculateResult(): float
    {
        return $this->currencyCalculator->calculate($this->src, $this->dst, $this->sum);
    }
}
