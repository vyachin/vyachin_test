<?php

namespace App\Service;

use Generator;

interface Adapter
{
    public function fetchRates(): Generator;
}
