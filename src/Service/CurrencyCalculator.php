<?php

namespace App\Service;

use App\Repository\RateRepository;

class CurrencyCalculator
{
    public function __construct(
        private readonly RateRepository $rateRepository
    ) {
    }

    /**
     * @throws CurrencyCalculatorException
     */
    public function calculate(string $src, string $dst, float $sum): float
    {
        $rate = $this->rateRepository->findOneBy(['src' => $src, 'dst' => $dst]);
        if ($rate) {
            if (!$rate->getRate()) {
                throw  new CurrencyCalculatorException('Division zero');
            }
            return $sum / $rate->getRate();
        }

        $rate = $this->rateRepository->findOneBy(['src' => $dst, 'dst' => $src]);
        if ($rate) {
            return $rate->getRate() * $sum;
        }

        throw  new CurrencyCalculatorException('Currency pair not found');
    }
}
