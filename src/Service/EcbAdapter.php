<?php

namespace App\Service;

use App\Dto\RateDto;
use Generator;

class EcbAdapter implements Adapter
{
    public function fetchRates(): Generator
    {
        $str = file_get_contents('https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml');
        $xml = simplexml_load_string($str);
        foreach ($xml->Cube->Cube->Cube as $item) {
            $dst = 'EUR';
            $src = (string)$item->attributes()['currency'];
            $rate = (float)$item->attributes()['rate'];
            yield new RateDto($src, $dst, $rate);
        }
    }
}
