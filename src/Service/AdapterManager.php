<?php

namespace App\Service;

use Generator;

class AdapterManager
{
    public function __construct(
        private readonly array $adapters,
    ) {
    }

    public function fetchRates(): Generator
    {
        foreach ($this->adapters as $adapter) {
            yield from $adapter->fetchRates();
        }
    }
}
