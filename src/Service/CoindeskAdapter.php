<?php

namespace App\Service;

use App\Dto\RateDto;
use Generator;

class CoindeskAdapter implements Adapter
{
    public function fetchRates(): Generator
    {
        $str = file_get_contents('https://api.coindesk.com/v1/bpi/currentprice.json');
        $json = json_decode($str, true);
        foreach ($json['bpi'] as $item) {
            $dst = 'BTC';
            $src = (string)$item['code'];
            $rate = (float)$item['rate_float'];
            yield new RateDto($src, $dst, $rate);
        }
    }
}
