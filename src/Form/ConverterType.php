<?php

namespace App\Form;

use App\Entity\Converter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConverterType extends AbstractType
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $currencyChoices = $this->getCurrencyChoices();

        $builder
            ->add('src', ChoiceType::class, [
                'label' => 'Из',
                'required' => true,
                'choices' => $currencyChoices,
            ])
            ->add('sum', MoneyType::class, [
                'label' => 'Сумма',
                'required' => true
            ])
            ->add('dst', ChoiceType::class, [
                'label' => 'В',
                'required' => true,
                'choices' => $currencyChoices,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Расчитать',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Converter::class,
        ]);
    }

    private function getCurrencyChoices(): array
    {
        $currencyCodes = $this->entityManager->getConnection()
            ->executeQuery('SELECT src FROM rate UNION SELECT dst FROM rate')
            ->fetchFirstColumn();
        sort($currencyCodes);

        return array_combine($currencyCodes, $currencyCodes);
    }
}
