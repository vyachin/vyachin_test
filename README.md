# Vladimir Vyachin test project

Запускаем проект локально

- Копируем docker-compose.override.sample.yml в docker-compose.override.yml
- Если нужно меняем номер порта web сервера в файле docker-compose.override.yml строка 6
- запускаем все контейнеры ```docker-compose up -d```
- подтягиваем php зависимости командой ```docker-compose exec php composer install```
- примеряем миграции к базе данных ```docker-compose exec php symfony console doctrine:migrations:migrate```
- загружаем котировки командой ```docker-compose exec php symfony console app:rate-fetch```
- консольный калькулятор запускаем
  командой ```docker-compose exec php symfony console app:rate-calculator EUR USD 100```
- веб версия калькулятора доступна по адресу http://127.0.0.1:8001/converter
