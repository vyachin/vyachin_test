up:
	docker-compose up -d --build

down:
	docker-compose down

make_migration:
	docker-compose exec php symfony console make:migration

migrate:
	docker-compose exec php symfony console doctrine:migrations:migrate

composer_upgrade:
	docker-compose exec php composer upgrade -W --no-progress
